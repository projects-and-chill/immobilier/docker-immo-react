ifeq (up,$(firstword $(MAKECMDGOALS)))
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(RUN_ARGS):;@:)
endif
ifeq (down,$(firstword $(MAKECMDGOALS)))
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(RUN_ARGS):;@:)
endif
ifeq (clear,$(firstword $(MAKECMDGOALS)))
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(RUN_ARGS):;@:)
endif



init:
	git clone git@gitlab.com:projects-and-chill/immobilier/nodejs-api-immobilier.git services/api-immobilier
	git clone git@gitlab.com:projects-and-chill/immobilier/react-front-immobilier.git services/front-immobilier
	git clone git@gitlab.com:models8/nginx-reverse-proxy.git services/reverse-proxy-immobilier

.PHONY: up
up:
	docker-compose -f docker-compose.$(RUN_ARGS).yml up

.PHONY: down
down:
	docker-compose -f docker-compose.$(RUN_ARGS).yml down -v

.PHONY: clear
clear:
	docker-compose -f docker-compose.$(RUN_ARGS).yml down -v --rmi "all"
